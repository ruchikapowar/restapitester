**Introduction**
=================
The project aims to elaborate the unit testing of a publicly available Rest API endpoint. The project showcases the 
TestNG framework usage along with RestAssured library for invoking api and validating the responses.

The project intends to test the rest endpoint `https://gorest.co.in/public/v1/users` . The api lists random users 
already present on the webside DB. The users aren't managed explicitly and aren't retained for any fixed duration and
can be present or removed by anyone using the api, this will cause the tests to fail randomly. Despite the flakiness
the test code will attempt to validate the use cases for the Rest api endpoint .

Prerequisites
-------------------
1. Gradle 7+ should be installed.
2. Java 11 must be installed.

Steps to execute
--------------------
In main directory execute `./gradlew test`


