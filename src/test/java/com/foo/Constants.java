package com.foo;

import java.util.UUID;

public class Constants {
    public static final String NON_EXISTENT_PARAM_VALUE = UUID.randomUUID().toString();
}
