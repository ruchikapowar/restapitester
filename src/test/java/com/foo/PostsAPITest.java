package com.foo;

import com.foo.model.Post;
import com.foo.model.User;
import com.foo.response.ResponseInfo;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Type;

/**
 * The type Posts api test.
 */
public class PostsAPITest {
    private static final String baseDomain = "https://gorest.co.in/public/v1";

    /**
     * Test primary users listing.
     */
    @Test
    public void testPrimaryUsersListing() {
        RequestSpecification request = RestAssured.given();
        Response response = request.get("/posts");
        ResponseInfo<Post> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });

        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() > 0 );
    }

    /**
     * Before method.
     */
    @BeforeMethod
    public void beforeMethod() {
        RestAssured.baseURI = baseDomain;
    }

    /**
     * After method.
     */
    @AfterMethod
    public void afterMethod() {

    }
}
