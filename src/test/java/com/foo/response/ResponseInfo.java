package com.foo.response;


import com.foo.model.User;
import io.restassured.response.Response;

import java.util.List;

/**
 * The type Response info.
 *
 * @param <T> the type parameter
 */
public class ResponseInfo<T> {
    private Meta meta;
    private List<T> data;

    /**
     * Gets meta.
     *
     * @return the meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Sets meta.
     *
     * @param meta the meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    public List<T> getData() {
        return data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(List<T> data) {
        this.data = data;
    }


}
