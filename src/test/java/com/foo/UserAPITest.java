package com.foo;

import com.foo.model.User;
import com.foo.response.ResponseInfo;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Type;
import java.util.List;

import static com.foo.Constants.NON_EXISTENT_PARAM_VALUE;

public class UserAPITest {
    private static final String baseDomain = "https://gorest.co.in/public/v1";

    @DataProvider(name = "existingUserDataProvider")
    public Object[][] existingUserDataProvider() {
        User user1 = new User();
        user1.setName("Esha Gill");
        user1.setEmail("esha_gill@kozey.net");
        user1.setGender("male");
        user1.setStatus("active");

        User user2 = new User();
        user2.setName("Kamlesh Pandey");
        user2.setEmail("kamlesh_pandey@nikolaus.io");
        user2.setGender("female");
        user2.setStatus("active");

        return new Object[][] {{user1}, {user2} };
    }

    @Test(groups = {"unit-test", "positive-tests"})
    public void testUsersListing() {
        RequestSpecification request = RestAssured.given();
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        verifyUserData(responseInfo);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() > 0 );
    }

    @Test(dataProvider = "existingUserDataProvider", groups = {"unit-test", "positive-tests"})
    public void testExistingUserSearchByName(User user) {
        RequestSpecification request = RestAssured.given();
        request.param("name", user.getName());
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        verifyUserData(responseInfo);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() == 1 );

    }

    @Test(groups = {"unit-test", "negative-tests"})
    public void testNonExistingUserSearchByName() {
        RequestSpecification request = RestAssured.given();
        request.param("name", NON_EXISTENT_PARAM_VALUE);
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() == 0 );
    }

    @Test(dataProvider = "existingUserDataProvider", groups = {"unit-test", "positive-tests"})
    public void testExistingUserSearchByEmail(User user) {
        RequestSpecification request = RestAssured.given();
        request.param("email", user.getEmail());
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        verifyUserData(responseInfo);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() > 0 );
    }

    @Test(groups = {"unit-test", "negative-tests"})
    public void testNonExistingUserSearchByEmail() {
        RequestSpecification request = RestAssured.given();
        request.param("email", NON_EXISTENT_PARAM_VALUE);
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() == 0 );
    }

    @Test(dataProvider = "existingUserDataProvider", groups = {"unit-test", "positive-tests"})
    public void testExistingUserSearchByGender(User user) {
        RequestSpecification request = RestAssured.given();
        request.param("gender", user.getGender());
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        verifyUserData(responseInfo);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() > 0 );
    }

    @Test(groups = {"unit-test", "negative-tests"})
    public void testUserSearchByIncorrectGender() {
        RequestSpecification request = RestAssured.given();
        request.param("gender", NON_EXISTENT_PARAM_VALUE);
        Response response = request.get("/users");
        Assert.assertEquals(response.getStatusCode(), 500);
    }


    @Test(dataProvider = "existingUserDataProvider", groups = {"unit-test", "positive-tests"})
    public void testExistingUserSearchByStatus(User user) {
        RequestSpecification request = RestAssured.given();
        request.param("status", user.getStatus());
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        verifyUserData(responseInfo);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() > 0 );
    }

    @Test(groups = {"unit-test", "negative-tests"})
    public void testUserSearchByIncorrectStatus() {
        RequestSpecification request = RestAssured.given();
        request.param("status", NON_EXISTENT_PARAM_VALUE);
        Response response = request.get("/users");
        Assert.assertEquals(response.getStatusCode(), 500);
    }

    @Test(dataProvider = "existingUserDataProvider", groups = {"unit-test", "positive-tests"})
    public void testExistingUserWithCorrectMultipleCriteria(User user) {
        RequestSpecification request = RestAssured.given();
        request.param("name", user.getName());
        request.param("email", user.getEmail());
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        verifyUserData(responseInfo);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() > 0 );
    }

    @Test(dataProvider = "existingUserDataProvider", groups = {"unit-test", "negative-tests"})
    public void testExistingUserWithIncorrectMultipleCriteria(User user) {
        RequestSpecification request = RestAssured.given();
        request.param("name", NON_EXISTENT_PARAM_VALUE);
        request.param("email", NON_EXISTENT_PARAM_VALUE);
        Response response = request.get("/users");
        ResponseInfo<User> responseInfo = response.body().as(new TypeRef<>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertTrue(responseInfo.getData().size() == 0 );
    }


    @BeforeMethod
    public void beforeMethod() {
        RestAssured.baseURI = baseDomain;
    }

    @AfterMethod
    public void afterMethod() {

    }

    private void verifyUserData(ResponseInfo<User> responseInfo) {
        List<User> users = responseInfo.getData();
        for (User user : users) {
            Assert.assertNotNull(user.getName());
            Assert.assertNotNull(user.getEmail());
            Assert.assertNotNull(user.getGender());
            Assert.assertNotNull(user.getStatus());
        }

    }
}
