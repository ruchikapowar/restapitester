package com.foo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The type Post.
 */
public class Post {

    private  int id;
    private  int userId;
    private  String title;
    private  String body;

    /**
     * Gets user id.
     *
     * @return the user id
     */
    @JsonProperty("user_id")
    public int getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    @JsonProperty("user_id")
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets body.
     *
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets body.
     *
     * @param body the body
     */
    public void setBody(String body) {
        this.body = body;
    }
}
